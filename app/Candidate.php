<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    // for mass asighnment
    protected $fillable = ['name','email','age'];

    //adding relation ships for DB
    // candidate belongs to a user
    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    //ex9
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }
// candidate has many interviews
    public function interviews()
    {
        return $this->hasMany('App\Interview');
    }
}
