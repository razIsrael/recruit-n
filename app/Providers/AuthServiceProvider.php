<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        // is the user able to change 
        Gate::define('change-status', function ($user,$candidate) {
            return $user->id === $candidate->user_id;
        });
        //is the user a maneger
        Gate::define('assign-user', function ($user) {
            return $user->isManager();
        });

        Gate::define('add-user', function ($user) {
            return $user->isAdmin();
        }); 
        
        //is maneger or admin
        Gate::define('add-interview', function ($user) {
           
            if($user->isAdmin()) return $user->isAdmin();

            if($user->isManager()) return $user->isManager();

            return false;



        });

    }
}
