@extends('layouts.app')

@section('title','Create Candidate')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Candidate</div>
                <div class="card-body">

                    <form action="{{action('CandidatesController@store')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Candidate Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name='name'>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="email" class="col-md-4 col-form-label text-md-right">Candidate Email</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name='email'>
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="email" class="col-md-4 col-form-label text-md-right">Candidate Age</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name='age'>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <br>
                                <input type="submit" name='submit' value='create Candidate'>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
