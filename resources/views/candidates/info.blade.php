@extends('layouts.app')

@section('title','Candidate Info')

@section('content')

<div class="container">

@if(Session::has('notallowed'))
    <div class='alert alert-danger'>
    {{Session::get('notallowed')}}
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Candidate details</h1>
            <table class="table table-dark">
                <!-- the table data -->
                <tr>
                    <td>Id</td>
                    <td>{{$candidate->id}}</td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>{{$candidate->name}}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>{{$candidate->email}}</td>
                </tr>
                <tr>
                    <td>Age</td>
                    <td>{{$candidate->age}}</td>
                </tr>
                <tr>
                    <td>Owner</td>
                    <td>
                        @if(isset($candidate->owner))
                        {{$candidate->owner->name}}
                        @else
                        No owner assigned yet
                        @endif
                    <td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$candidate->status->name}}</td>
                </tr>
                <tr>
                    <td>Created</td>
                    <td>{{$candidate->created_at}}</td>
                </tr>
                <tr>
                    <td>Updated</td>
                    <td>{{$candidate->updated_at}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-8">
            <form method="POST" action="{{ route('candidates.changestatusfrominfo') }}">
                @csrf
                <div class="form-group row">
                    <label for="department_id" class="col-form-label">Move to status</label>
                    <div class="col-md-6">
                        <select class="form-control" name="status_id">
                            @foreach (App\Status::next($candidate->status_id) as $status)
                            <option value="{{ $status->id }}">
                                {{ $status->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <input name="id" type="hidden" value='{{$candidate->id}}'>
                    <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Change status
                            </button>
                    </div>
            </form>

        </div>
    </div>
</div>


@endsection
