<?php

use Illuminate\Database\Seeder;

class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([
            [
                'date' => date("Y-m-d H:i:s"),
                'description' => 'Wouldn’t it be great if you knew exactly what questions a hiring manager would be asking you in your next job interview?

                We can’t read minds, unfortunately, but we’ll give you the next best thing: a list of more than 40 of the most commonly asked interview questions, along with advice for answering them all.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
        ],
        [
            'date' => date("Y-m-d H:i:s"),
             'description' => 'While we don’t recommend having a canned response for every interview question (in fact, please don’t), we do recommend spending some time getting comfortable with what you might be asked, what hiring managers are really looking for in your responses, and what it takes to show that you’re the right person for the job.',
             'created_at' => date("Y-m-d H:i:s"),
              'updated_at' => date("Y-m-d H:i:s")
        ],
        [
            'date' => date("Y-m-d H:i:s"),
             'description' => 'Consider this list your interview question and answer study guide.',
             'created_at' => date("Y-m-d H:i:s"),
              'updated_at' => date("Y-m-d H:i:s")
        ]
    
   ] );
    }
}
