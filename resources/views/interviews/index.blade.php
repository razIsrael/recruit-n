@extends('layouts.app')

@section('title', 'Interviews')

@section('content')


@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
       

            <h1>List of interviews</h1>
            @if(isset($interviews))
                           
            <div><a href="{{url('/interviews/create')}}" class="btn btn-info" role="button">Add New Interview</a></div>
    <br>

            <table class="table table-dark">
                <tr>
                    <th>id</th>
                    <th>Date</th>
                    <th>Discription</th>
                    <th>Hoste</th>
                    <th>Candidate</th>
                </tr>
                <!-- the table data -->
                @foreach($interviews as $interview)
                <tr>
                    <td>{{$interview->id}}</td>
                    <td>{{$interview->date}}</td>
                    <td>{{$interview->description}}</td>
                    <td>{{$interview->user->name}}</td>

                    <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(isset($interview->candidate_id))
                            {{$interview->candidate->name}}
                            @else
                            Assign Candidate
                            @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($candidates as $candidate)
                            <a class="dropdown-item"
                                href="{{route('interview.changeCandidate',[$interview->id,$candidate->id])}}">
                                {{$candidate->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </td>

                </tr>
                @endforeach
            </table>

                            @else
                            <div class='alert alert-danger'>
                            you have no interviews
                            </div>
                           
                            @endif
        </div>
    </div>
</div>
@endsection
