<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User; 
use App\Department;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use App\Candidate;
use App\Status;
use App\Interview;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews = Interview::all(); 
        $candidates = Candidate::all();     
        return view('interviews.index', compact('interviews','candidates'));
    }

    public function changeCandidate($iid,$cid = null)
    {
        if(Gate::allows('assign-user'))
        {
        $interviews = Interview::findOrFail($iid);
        $interviews->candidate_id = $cid;
        $interviews->save();
        }else{
            Session::flash('notallowed','you are not authorized to change candidate because you are not the Maneger');
        }
        
      return back();
    }

    public function myInterviews()
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;

        $candidates = Candidate::all();    

        return view('interviews.index', compact('interviews','candidates'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if(Gate::allows('add-interview'))
        {
            return view('interviews.create');
        }else{
            Session::flash('notallowed', 'You are not allowed to create interview');
            return back();
        }

        return view('interviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // saving data to DB, request coms from the form html
       $interview = new Interview();

       $interview->description = $request->description;
     $interview->date = date("Y-m-d H:i:s");
     $interview->candidate_id = null;
     $interview->user_id = 1;
      
        
          $interview->save();
          return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
