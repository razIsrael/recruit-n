<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
// for mass asighnment
protected $fillable = ['daete','description','candidate_id','user_id'];

    // Interview belongs to a Candidate
    public function candidate()
    {
        return $this->belongsTo('App\Candidate', 'candidate_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
