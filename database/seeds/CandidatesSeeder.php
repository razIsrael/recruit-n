<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
//use Faker\Factory as Faker;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->insert([
            
            'name' => Str::random(6).' '.Str::random(6),
            'email' => Str::random(7).'@gmail.com',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
            
        ]);
    }
}

            // $table->id();
            // $table->string('name', 20);
            // $table->string('email', 30);
            // $table->timestamps();