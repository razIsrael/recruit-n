@extends('layouts.app')

@section('title', 'users')

@section('content')

@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <h1>List of Users</h1>
            <table class="table table-dark">
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <!-- <th>Role</th> -->
                    <th>Department</th>
                </tr>
                <!-- the table data -->
                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <!-- <td>rolse</td> -->
                    <td>{{$user->department->department}}</td>

                    @can('add-user')
                    <td>
                        <a href="{{route('users.edit',$user->id)}}">Edit</a>
                    </td>
                    <td>
                        <a href="{{route('user.delete',$user->id)}}">Delete</a>
                    </td>
                    @endcan
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection
