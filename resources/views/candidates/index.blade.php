@extends('layouts.app')

@section('title','Home Page')

@section('content')
<div class="content">

    @if(Session::has('notallowed'))
    <div class='alert alert-danger'>
        {{Session::get('notallowed')}}
    </div>
    @endif

    <h1 class="text-center">list of candidates</h1>
    <div><a href="{{url('/candidates/create')}}" class="btn btn-info" role="button">Add New Candidate</a></div>
    <br>
    <div class="table-responsive-md">
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col"> <a href="{{route('candidate.sortAge')}}">Age</a></th>
                    <th scope="col">Status</th>
                    <th scope="col">Owner</th>
                    <th scope="col">Created At</th>
                    <th scope="col">Updated</th>
                    <th scope="col"> </th>
                    <th scope="col"> </th>
                    <th scope="col"> </th>
                </tr>
            </thead>
            <!-- table data using blade-->
            @foreach($candidates as $candidate)
            @if($candidate->status->name == 'not fit' || $candidate->status->name == 'not fit professionally')
            <tr style="color:red">
                @elseif($candidate->status->name == 'accepted to work')
            <tr style="color:green">
                @else
            <tr>
                @endif
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td>{{$candidate->age}}</td>
                <td>
                    <div class="dropdown">
                        <!-- insert ronis code -->
                        @if (null != App\Status::next($candidate->status_id))
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(isset($candidate->status_id))
                            {{$candidate->status->name}}
                            @else
                            before interview
                            @endif
                        </button>
                        @else
                        {{$candidate->status->name}}
                        @endif

                        @if(App\Status::next($candidate->status_id) != null)
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach(App\Status::next($candidate->status_id) as $status)
                            <a class="dropdown-item"
                                href="{{route('candidate.changestatus',[$candidate->id,$status->id])}}">
                                {{$status->name}}</a>
                            @endforeach
                        </div>
                        @endif
                    </div>
                </td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(isset($candidate->user_id))
                            {{$candidate->owner->name}}
                            @else
                            Assign Owner
                            @endif
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach($users as $user)
                            <a class="dropdown-item"
                                href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">
                                {{$user->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
                <td><a href="{{action('CandidatesController@info',$candidate->id)}}">Info</a></td>
                <td><a href="{{action('CandidatesController@edit',$candidate->id)}}"><button type="button"
                            class="btn btn-success">Edit</button></a></td>
                <td><a href="{{route('candidate.delete', $candidate->id)}}"><button type="button"
                            class="btn btn-danger">Delete</button></a></td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
