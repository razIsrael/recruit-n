<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//rout for all my candidates
Route::resource('candidates', 'CandidatesController')->middleware('auth');
// overiding delet route
Route::get('candidades/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');
//sending to my function of choosing user
Route::get('candidades/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
//ex9 adding status
Route::get('candidades/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus');
//ex 11 new rout for page myCandidate
Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidate.myCandidates')->middleware('auth');
//exmple test vew sorted
Route::get('candidatesSortByAge', 'CandidatesController@sortAge')->name('candidate.sortAge')->middleware('auth');
//exmple test info rout 
Route::get('candidades/info/{cid}', 'CandidatesController@info')->name('candidate.info');
//ronis code
Route::post('candidates/changestatus/', 'CandidatesController@changeStatusFromInfo')->name('candidates.changestatusfrominfo')->middleware('auth');


//all user routs
Route::get('users', 'UsersController@index')->name('users.index')->middleware('auth');
Route::get('users/{id}/edit', 'UsersController@edit')->name('users.edit')->middleware('auth');
Route::get('users/{id}/delete/', 'UsersController@destroy')->name('user.delete')->middleware('auth');
Route::post('users/{id}/update/', 'UsersController@update')->name('users.update')->middleware('auth');

Route::get('users/add', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('users.add')->middleware('auth');
Route::post('users/create', '\App\Http\Controllers\Auth\RegisterController@adduser')->name('users.create')->middleware('auth');

//all interviews routs
Route::get('interviews', 'InterviewsController@index')->name('interviews.index')->middleware('auth');
Route::get('interviews/changecandidate/{cid}/{uid?}', 'InterviewsController@changeCandidate')->name('interview.changeCandidate');
Route::get('myinterviews', 'InterviewsController@myInterviews')->name('interview.myInterviews')->middleware('auth');

Route::get('interviews/create', 'InterviewsController@create')->name('interviews.create')->middleware('auth');
Route::post('interviews/stor', 'InterviewsController@store')->name('interviews.add')->middleware('auth');
// Route :: get('/student/{id}', function($id = 'No student found'){
//     return 'Hello Student id Number '.$id;
// });

//adding number is optenal because of ?
Route :: get('/car/{id?}', function($id = null){
    if (isset($id)){
        return "This is Car Number $id".'This is Car Number '.$id;
        //'This is Car Number '.$id 
    }else{
        return 'there is No Car Id selected';
    }
    
});

//  Route::get('/comment', function () {
//      return view('comment');
//  });

Route::get('/comment/{id?}', function ($id = 'non selected') {
    return view('comment',compact('id'));
});

// homework ex 5
Route::get('/users/{name?}/{email}', function ($name = 'name missing', $email = null) {
    return view('users',compact('name','email'));
    
});
Route::get('/users/{email}', function ($email = null, $name = 'name missing') {
    return view('users',compact('name','email'));
    
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
