@extends('layouts.app')

@section('title','Create Interview')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Interview</div>
                <div class="card-body">

                <form action="{{action('InterviewsController@store')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Description of in</label>
                            <div class="col-md-6">
                                <input type="textbox" class="form-control" name='description'>
                            </div>
                        </div>

                        
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <br>
                                <input type="submit" name='submit' value='Add Interview'>
                            </div>
                        </div>

                    </form>

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
