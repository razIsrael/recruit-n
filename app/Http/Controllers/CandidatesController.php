<?php

namespace App\Http\Controllers;

// like import
use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

// full name is "App\Http\Controllers\CandidatesController"
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all();// get all from chart using eliquent
        $users = User::all();//get all users
        $statuses = Status::all();//ex9
        return view('candidates.index', compact('candidates','users','statuses'));//send data to index
    }

    public function info($cid)
    {
        $candidate = Candidate::findOrFail($cid);
        return view('candidates.info', compact('candidate'));
    }
    public function changeStatusFromInfo(Request $request){
        $cid = $request->id;
        $sid = $request->status_id;
        $candidate = Candidate::findOrFail($cid);
        if(Gate::allows('change-status', $candidate))
        {
            $from = $candidate->status->id;
            if(!Status::allowed($from,$sid)) return redirect('candidates');        
            $candidate->status_id = $sid;
            $candidate->save();
        }else{
            Session::flash('notallowed', 'You are not allowed to change the status of the user becuase you are not the owner of the user');
        }
        return redirect('candidates');
    }


    public function myCandidates()
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $candidates = $user->candidates;
       // $candidates = Candidate::all();// the difrens between pages
        $users = User::all();//get all users
        $statuses = Status::all();//ex9
      return view('candidates.index', compact('candidates','users','statuses'));//send data to index
    }

    public function sortAge()
    {
        //returen to view filtering candidates by age
        $candidates = Candidate::all();
        $candidates = $candidates->sortBy('age');//$collection->sortBy('field', [], true); // true for descending

        $users = User::all();
        $statuses = Status::all();

      return view('candidates.index', compact('candidates','users','statuses'));//send data to index
    }

    public function changeUser($cid,$uid = null)
    {
        if(Gate::allows('assign-user'))
        {
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        }else{
            Session::flash('notallowed','you are not authorized to change userbecause you are not the Maneger');
        }
        
      //return redirect('candidates');//send to index
      return back();
    }
    public function changeStatus($cid,$sid)
    {
        $candidate = Candidate::findOrFail($cid);
       //the user is provided in gate
        if(Gate::allows('change-status', $candidate))
        {
            $from = $candidate->status->id;
            if(!Status::allowed($from,$sid)) return redirect('candidates');
            $candidate->status_id = $sid;
            $candidate->save();
        }else{
            Session::flash('notallowed','you are not authorized to change status of user because you are not the owner of user');
        }

        return back();//send to index
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // saving data to DB, request coms from the form html
       $candidate = new Candidate();
    //    $candidate->name = $request->name;
    //    $candidate->email = $request->email;
       // can also use mass assinment
      // $candidate->create($request->all());
      // added this because new status_id needs a starting number
       $can =  $candidate->create($request->all());
       $can->status_id = 1;
       $can->save();
       return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $candidate = Candidate::findOrFail($id);
       // return view('candidates.edit',compact('candidate','id'));
       return view('candidates.edit',compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $candidate = Candidate::findOrFail($id);
       
        $candidate->update($request->all());

        // $candidate->name = $request->name;
        // $candidate->email = $request->email;
        // $candidate->save();
        return redirect('candidates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');
    }
}
